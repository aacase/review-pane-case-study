# Redesigning the Review Pane

## The Code
The updated Review Pane prototype was written in React 16 with MobX serving as the state management/data store.

React Table, Victory Charts, Moment, React Currency Format, Toggle, and React Three State Checkbox were the third party dependencies used.


## Backstory
When the review pane was first written, it was a major shift away from our old concept of a cart of many carts. Some of us will recall that it was somewhat confusing what exactly made up a cart. Enter the first version of the Review Pane. 

Initially we offered a view of Reseller Events, with price changes nested underneath. After some real world testing we added the ability to filter based on price change type (Manual vs Automated), the ability to sort event order by date and available quantity, and the addition of the search by tags feature.

Initially we found that including all of the listing data as a property of the event was causing some serious bottlenecks with regard to the application's front end. Leo refactored this backend piece, and the UI then lazy loaded the listings on expansion. Performance was improved, but there are still some serious bottlenecks when it comes to user workflow.

![The State of Thing](https://i.imgur.com/4g99Pmu.png)

## Pain Points


- Finding your listings
    - It can be next to impossible to find the listing you're looking for. For events that have hundreds of listings in the cart (Primesport NFL), it can take a lot of scrolling to get to your listing.

- Keeping your place in the table
    - This is a major design flaw. Once listings are expanded you have to scroll around a lot to accomplish your work, and if you're trying to go through events in a sequence, the work feels disjointed.

- Automated vs Manual filter is not intuitive
    - We default this filter to all listings, which just has the words "View: All". This does nothing to tell the user on first glance that the filter has anything to do with price changes or how they were made.

- What do these prices mean?
    - Once a set of listings has been expanded, the user has to leave the review pane/click over to a new event on the maps page to gather information.

- Changing Prices on the go
    - We have no mobile price change offering right now for resellers, and what we could currently offer doesn't give much insight into the data around our prices. 

## The Redesign

  In the 4.9.0 release, the webpack build system was added to the resale project. The legacy Angular code that makes up the current resale application was rewritten to es6 style modular code. Gradually we will phase out the Angular 1+ pieces of the application in leiu of more performant and more modern libraries. 

  One of the first pieces we'll rewrite is the navigation, and within that navigation sits the review pane. This presents us with a bit of a dilema initially. When then nav is refactored, we'll need to conditionally show and hide the reivew pane based on the user's location within the application. (The Angular Review Pane as it's currently written will not work in the KPI dashboard). The next step after the 4.10 release should be the refactoring of the review pane. 

  The benefit of modern Javascript is that it's much easier and faster to write, and I've already done much of the heavy lifting. All these screens are from a working prototype I constucted over the course of about 15 hours. The data is real, save for the Event Pie, though the rendering of it is done via code and could easily have real data plugged in. No backend pieces are required unless we'd like to add new functionality. 

  And here's the best part: As a side effect of using react, new code all becomes mobile friendly with very minimal tweaks for conditionally showing and hiding components. This means mobile price changing. 

### The Pane:
![overview](https://i.imgur.com/xpFw6Ln.png)

Overall this should feel very familiar to our users. Notably missing is the sort by date quantiy sort dropdown. Instead, these actions are done via table header sorts. 

![header sorting](https://i.imgur.com/n77PSin.png)

You'll also notice that the automated vs manual sort has been replaced with an Automated only toggle. 

![manual](https://i.imgur.com/juKUt8W.png)

![automatic](https://i.imgur.com/VskJnP5.png)

Pendo usage data does not suggest that anyone is using this filter.

![no clicks](https://i.imgur.com/TBnNuAc.png)

The idea is that users can drill down to check automated listings if they need granularity. They know which manual price changes were made because...they made them. Perhaps a color indicator in the table is the real way to differenciate, but filtering by price type isn't a priority for our users currently, possibly because it isn't clear they can. This is an explicit way to make sure they know.

![table](https://i.imgur.com/oSUN7Uv.png)

Current event selected on the map page will show as a blue highlight as it does currently. Column headers are sortable and resizeable. The checkbox state is dependent on the number of child listings selected, as it is the current Review Pane. 

You'll notice that the listing column sorts have been removed from this view. This is intentional. 

![listings](https://i.imgur.com/JBpYW5y.png)

Here's the major redesign. Upon expansion, the user is greated with the event pie and some other data driven text, possibly current sell through and ROI for the current event.

![pie](https://i.imgur.com/s5cCItO.png)
This is a donut chart made up of available, unshared, and sold listings information for the user's listings on the current event. This is just a React component and can be replaced with literally anything we can think of.

![listings table](https://i.imgur.com/qlmD00c.png)
This table is where our listing level sorts moved. It was important to solve the challenge of getting lost in your listings. By going with a default pagination style of 5 listings per page, users maintain their place in their workflow. Once they are done with the listings in this event, it's one scroll down motion to get to their next event. 

![dropdown](https://i.imgur.com/WPzsONP.png)

In events with large amounts of listings such as the one in the example, users might not want to click through 43 pages of listings. The users can expand the number of listings on the current page up to 100 per page (this is configurable, it can be a million). The table data has a fixed height. Selecting a larger amount than the default activates scroll overflow up to the number selected.

## Takeaways
With this redesign, a price changer can (From anywhere; this can be optimized for mobile) easily log in and start reviewing their automated price changes with minimal effort to locate thier listings and the relevant data they need to price them. 

This design be extended to include additional information (Listing diagnotics on automated price hover) with minimal developer effort.

To recreate current Review Pane functionality with submission and select all state persistance would take approximately 3-4 developer days. 



