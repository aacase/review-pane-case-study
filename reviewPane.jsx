import React, { Component } from 'react';
import ReactTable from "react-table";
import ReviewPaneListings from './reviewPaneListings';
import { observable, action } from "mobx";
import { observer } from "mobx-react";
import Toggle from 'react-toggle'
import Checkbox from 'react-three-state-checkbox'
import {VictoryPie} from 'victory'
import Moment from 'moment';
import 'moment-timezone';
import scss from './reviewPane.scss'


import ReviewPaneStore from './stores/reviewPaneStore'
import ReviewPaneService from './reviewPaneService'

@observer
class ReviewPane extends Component {

    constructor(props) {
    super(props);
/**
|--------------------------------------------------
1.  Create the service with correct URLs to fetch data
2.  Make a store for tracking state changes tied to this cart.
3.  Set the inital event list to the created events in the store
4.  As price changes are submitted and rejected, this will update 
automagically. 
|--------------------------------------------------
*/   
     this.reviewPaneService = new ReviewPaneService(this.props.reactReseller);
     this.priceChangeStore =  new ReviewPaneStore (this.reviewPaneService);
     this.priceChangeStore.fetchPriceChanges();
     this.filterToAutomated
     this.state = {}
     this.state.onlyAutomated = false;
        this.columns = [
          {
            Header:"",
            width: 30,
            Cell: row => (
              <Checkbox
                checked={false}
                indeterminate={true}
              />
              )
          },
             {
                Header: "Event",
                accessor: "eventDescription"
              },
              {
                id: 'momentDate',
                Header: "Date",
                width:160,
                accessor: d => {
                  return Moment(d.eventDate)
                  .local()
                  .format("MMMM Do, h:mm a")
                }
              },
              {
                Header: "Qty",
                accessor: "available",
                width: 60,
              },
        ]
        this.events

    }

    componentDidMount() {

    }
    render() {
/**
|--------------------------------------------------
| REALLY REALLY IMPORTANT
a Mobx "observable" is not supported by ReactTable.
convert it to a normal array using spread syntax
(You can also use the slice method, but we can use
the sugary sytanx so please do)
|--------------------------------------------------
*/
    let store = this.priceChangeStore
    let data = [...store.events];
    return (
      <div>
        <div className="toggleHolder">
        <span>Automated Only</span>
        <Toggle defaultChecked={this.state.onlyAutomated} onChange={console.log('this will propegate to other spots')}/>
        </div>

        <ReactTable
          data={data}
          columns={this.columns}
          showPagination = {false}
          className="-striped -highlight"
          SubComponent={row => {
            return (
              <div>
              <div className="reviewPaneChart">
                <div className="reviewPie">
                  <VictoryPie
                      innerRadius={100}
                      colorScale={["black","#7AB82B", "red"]}
                      data={[
                        { x: 1, y: 5, label: "Sold" },
                        { x: 2, y: 1, label: "Unsold" },
                        { x: 3, y: 2, label: "Unlisted" }
                      ]}
                    />
                </div>
                <div className='dataList'>
                  <ul>
                    <li>We can list anything we want here</li>
                    <li>I was thinking an Event Pie for the event</li>
                    <li>We could also load an event map</li>
                  </ul>
                </div>
              </div>
                <ReviewPaneListings parentStore={store} reactReseller={this.props.reactReseller} eventId={row.original.eventId}></ReviewPaneListings>
              </div>
            );
          }}
        />
      </div>
    );
  }
}

export default ReviewPane