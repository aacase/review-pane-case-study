class ReviewPaneListingsService  {

    constructor(resellerId, eventId) {
        this.getExpandedListingsUrl = "THEURLTHATGETSYOURDATA" + resellerId, eventId
        this.expandedListings = []
    }

    getExpandedListings() {
        return fetch(this.getExpandedListingsUrl).then(response => response.json())
    }
}
export default ReviewPaneListingService