import React, { Component } from "react";
import ReactTable from "react-table";
import CurrencyFormat from 'react-currency-format'
import { observable, action } from "mobx";
import { observer } from "mobx-react"
import ReviewPaneListingDetailStore from './stores/reviewPaneListingDetailStore'
import ReviewPaneListingService from './reviewPaneListingService'
import Checkbox from 'react-three-state-checkbox'

@observer
class ReviewPaneListings extends Component {
  constructor(props) {
    super(props);
    this.listings;
    this.parentStore = this.props.parentStore // This is where we push selected listings.
    this.reviewPaneListingService = new ReviewPaneListingService(this.props.reactReseller, this.props.eventId);
    this.expandedListingsStore  =  new ReviewPaneListingDetailStore (this.reviewPaneListingService);
    this.expandedListingsStore.fetchListings();
    this.columns = [
      {
        Header:"",
        accessor: "selectedListings",
        width:20,
        Cell: row => (
          <Checkbox
            checked={false}
          />
          )
      },
      {
        id: "area",
        Header: "Zone",
        accessor: "area",
        width: 240
      },
      {
        Header: "Qty",
        accessor: "availableQuantity",
        width: 35
      },
      {
        Header: "Current",
        accessor: "price",
        width: 60,
/**
 |--------------------------------------------------
| We can literally put components everywhere. Go wild. 
| 
| A style note here, when a component's props 
start getting fat, break it up like so: 
|--------------------------------------------------
*/
        Cell: row => (
          <CurrencyFormat
              value={row.value}
              displayType={"text"}
              decimalScale={2}
              fixedDecimalScale={true}
              thousandSeparator={true}
              prefix={"$"}
          />
          )
      },
      {
        Header: "New",
        accessor:"priceChange.proposedPrice",
        width: 60,
        Cell: row => (
        <CurrencyFormat
            value={row.value}
            displayType={"text"}
            decimalScale={2}
            fixedDecimalScale={true}
            thousandSeparator={true}
            prefix={"$"}
        />
        )
      },
/**
|--------------------------------------------------
| The price change percentage field is a really intertesting
look into how MobX works. The original listing object as looked up
has no concept of what the price change percentage is. Because
this data is rendered from a store and not directly from the network
request, we can transform it in all kinds of cool ways.

See reviewPaneListingModel.js to see how this value is made.
|--------------------------------------------------
*/
      {
        id: "change",
        Header: "Change",
        width: 90,
        accessor:"priceChangePercentage",
      }
    ];
  }

  componentDidMount() {

  }

  render() {
    let store = this.expandedListingsStore
    let data = [...store.expandedListings];
    console.log(store)
    return (
      <div>
        <ReactTable 
        data={data} 
        columns={this.columns}  
        defaultPageSize={5}  
        showPagination={true}
        style={{
          height: "280px" // This will force the table body to overflow and scroll, since there is not enough room
        }} />
      </div>
    );
  }
}
export default ReviewPaneListings;
