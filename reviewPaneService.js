class ReviewPaneService {
    /**
    |--------------------------------------------------
    | Class getters and setters can't take params,
    but their sub methods can. So we use the contructor to
    pass props. 
    |--------------------------------------------------
    */
    constructor(resellerId) {
        this.getPriceChangesUrl = 'THEURLTHATGETSYOURDATA' + resellerId
        this.priceChanges = [];
    }

    getPriceChanges() {
       return  fetch(this.getPriceChangesUrl).then(response => response.json())
    }
    
}

export default ReviewPaneService