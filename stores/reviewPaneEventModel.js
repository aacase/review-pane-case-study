import { observable, computed } from "mobx";

export default class ReviewPaneEventModel {
  @observable eventId
  @observable eventDescription;
  @observable eventDate
  @observable listings
  @observable selectAllState = false;
 
  @computed
  get selectedListingsCount() {
    return this.listings.filter(listing => !listing.selected).length;
  }

  @computed
  get available() {
    //This is where the automatic pricing vs manual filtering will happen
    return this.listings.length
  }

  constructor(event, selectAllState) {
    this.eventId = event.eventId
    this.eventDescription = event.eventDescription;
    this.listings = event.listings
    this.eventDate = event.eventDate
    this.selectAllState = selectAllState
  }
}