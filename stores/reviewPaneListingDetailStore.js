import {observable, computed, action, runInAction} from 'mobx';
import ReviewPaneListingModel from './reviewPaneListingModel';

class ReviewPaneListingDetailStore {
    constructor(reviewPaneListingService){
        this.reviewPaneListingService = reviewPaneListingService
        this.reviewPaneListingService.getExpandedListings()
        this.expandedListings = this.reviewPaneListingService.expandedListings
    }

    @observable expandedListings = []
    @observable state = "pending" // "pending" / "done" / "error"
    @action
    fetchListings() {
        this.state = "fetching"
        this.reviewPaneListingService.getExpandedListings().then(
            listings => {
                runInAction(() => {
                    listings.forEach(listing => {
                       this.addListing(listing,false);
                    });
                    this.state = "done"
                })
            },
            error => {
                // the alternative ending of this process:...
                runInAction(() => {
                    this.state = "error"
                })
            }
        )
    }

    @action
    addListing(listing) {
      console.log('adding '+listing.area+ ' to the store');  
      this.expandedListings.push(new ReviewPaneListingModel(listing, false));
    }
}

export default ReviewPaneListingDetailStore 
