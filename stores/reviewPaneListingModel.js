import { observable, computed } from "mobx";

export default class ReviewPaneListingModel {
  @observable listing
  @observable selected = false;
 
  @computed
  get selectedListingsCount() {
    return this.listings.filter(listing => !listing.selected).length
  }

  @computed // let's us call lising.priceChangePercenage in react.
  get priceChangePercentage() {
    let change
    if (this.sellerPrice)(
       change =Math.round(((this.priceChange.price)/this.price)*100)-100
    )
    else{
     change =  Math.round(((this.priceChange.price)/this.price * (100 + this.markup)/100)*100)-100
    }
    return Math.abs(change) + '%'
  }
  

  constructor(listing) {
    this.area = listing.area
    this.availableQuantity = listing.availableQuantity
    this.price =listing.price
    this.selected = listing.selected
    this.priceChange = listing.priceChange
    this.markup = listing.markup
    this.proposerId = this.proposerId
    this.proposerName = this.proposerName
  }
}