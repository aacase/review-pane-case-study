import {observable, computed, action, runInAction} from 'mobx';
import ReviewPaneEventModel from './reviewPaneEventModel';
/**
|--------------------------------------------------
| A lot of things going on here. First, this is a 
mobx store. I opted to go this route because it's
a bit easier to understand than redux, and our app's
state just isn't complicated enough to need redux right now.
|
| The core concepts of mobX are these: 
Stores contain the state of observable data.
When the state of that data changes as the result of an 
action, let’s say a price selection for submission for example, 
An update is sent to the store and the store data is 
replaced. The store then feeds the change any component 
that is currently subscribed to it, re-rendering only what is required.
|
| The best part is we never need to use react's setState method,
 which is arguably the most confusing part of react

TLDR: Events invoke actions, actions modify the store, compute functions run , the DOM reacts. 

|--------------------------------------------------
*/



class ReviewPaneStore {
    constructor(reviewPaneService){
        // we need a way to pass props for lookups.
        this.reviewPaneService = reviewPaneService
        this.reviewPaneService.getPriceChanges()
        this.priceChanges = this.reviewPaneService.priceChanges
    }

    @observable events = []
    @observable selectedListings =[]
    @observable state = "pending" // "pending" / "done" / "error"
    @action
    fetchPriceChanges() {
        this.events = []
        this.state = "fetching"
        this.reviewPaneService.getPriceChanges().then(
            events => {
                // put some filtering actions here if you want.
                // put the 'final' modification in an anonymous action
                runInAction(() => {
                    events.forEach(event => {
                       this.addEvent(event,false);
                    });
                    this.state = "done"
                })
            },
            error => {
                // the alternative ending of this process:...
                runInAction(() => {
                    this.state = "error"
                })
            }
        )
    }
    // not used yet
    @action
    updateCheckedListings(listing){
        console.log ('updating '+ listing);
        this.selectedListings.push(listing);
    }

    @action
    addEvent(event) {
      console.log('adding '+event.eventDescription+ ' to the store');  
      this.events.push(new ReviewPaneEventModel(event, false));
    }
    
/**
|--------------------------------------------------
| @action methods in stores should be where our 
 services should be injected. NOT in react components.
 React components should not know what goes into getting
 their data, only what to do with it. Getters and setters
 go in stores.

| Instead components will call store. 
|--------------------------------------------------
*/


}

export default ReviewPaneStore
